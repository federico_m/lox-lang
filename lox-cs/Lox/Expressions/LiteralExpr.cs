﻿using Lox.Expressions.Visitors;

namespace Lox.Expressions;

public record class LiteralExpr(object? Value = null) : Expr
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}
