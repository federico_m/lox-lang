using Lox.Expressions;
using Lox.Statements.Visitors;

namespace Lox.Statements;

public record class PrintStmt(Expr Expression) : Stmt
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}