﻿using System.Data;
using System.Reflection;
using Lox.Expressions;
using Lox.Statements;
using Lox.Tokens;

namespace Lox;

public class Parser(List<Token> tokens)
{
    private readonly List<Token> _tokens = tokens;
    private int _current = 0;

    public List<Stmt> Parse()
    {
        var res = new List<Stmt>();

        while (!IsAtEnd())
        {
            if (Declaration() is Stmt stmt)
            {
                res.Add(stmt);
            }
        }

        return res;
    }

    private Stmt? Declaration()
    {
        try
        {
            if (Match(TokenType.CLASS))
            {
                return ClassDeclaration();
            }

            if (Match(TokenType.FUN))
            {
                return FunctionDeclaration("function");
            }

            if (Match(TokenType.VAR))
            {
                return VarDeclaration();
            }

            return Statement();
        }
        catch (ParseError error)
        {
            Synchronize();
            return null;
        }
    }

    private ClassStmt ClassDeclaration()
    {
        var name = Consume(TokenType.IDENTIFIER, "Expected class name");

        VariableExpr? superclass = null;
        if (Match(TokenType.LESS))
        {
            // Superclass indication
            Consume(TokenType.IDENTIFIER, "Expected superclass identifier.");
            superclass = new VariableExpr(Previous());
        }

        Consume(TokenType.LEFT_BRACE, $"Expect '{{' after class name.");

        var methods = new List<FunctionStmt>();
        while (!Check(TokenType.RIGHT_BRACE) && !IsAtEnd())
        {
            methods.Add(FunctionDeclaration("method"));
        }

        Consume(TokenType.RIGHT_BRACE, $"Expect '}}' after class body.");

        return new ClassStmt(name, methods, superclass);
    }

    private VarStmt VarDeclaration()
    {
        var name = Consume(TokenType.IDENTIFIER, "Expected variable name.");

        var initializer = Match(TokenType.EQUAL) ? Expression() : null;
        Consume(TokenType.SEMICOLON, "Expected ';' at the end of variable declaration.");

        return new VarStmt(name, initializer);
    }

    private FunctionStmt FunctionDeclaration(string kind)
    {
        var name = Consume(TokenType.IDENTIFIER, "Expect " + kind + " name.");
        var paramList = new List<Token>();

        Consume(TokenType.LEFT_PAREN, $"Expect '(' after {kind} {name}.");
        if (!Check(TokenType.RIGHT_PAREN))
        {
            do
            {
                if (paramList.Count > 255)
                {
                    Error(Peek(), "Can't have more than 255 parameters.");
                }

                paramList.Add(Consume(TokenType.IDENTIFIER, "Expected parameter name"));
            } while (Match(TokenType.COMMA));
        }
        Consume(TokenType.RIGHT_PAREN, $"Expect ')' after parameter list for {kind} {name}.");
        Consume(TokenType.LEFT_BRACE, $"Expected '{{' after parameter list for {kind} {name}.");

        var body = Block();

        return new FunctionStmt(name, paramList, body);
    }

    private Stmt Statement()
    {
        if (Match(TokenType.IF))
        {
            return IfStatement();
        }

        if (Match(TokenType.WHILE))
        {
            return WhileStatement();
        }

        if (Match(TokenType.FOR))
        {
            return ForStatement();
        }

        if (Match(TokenType.PRINT))
        {
            return PrintStatement();
        }

        if (Match(TokenType.RETURN))
        {
            return ReturnStatement();
        }

        if (Match(TokenType.LEFT_BRACE))
        {
            return new BlockStmt(Block());
        }

        return ExpressionStatement();
    }

    private List<Stmt?> Block()
    {
        var statements = new List<Stmt?>();

        while (!Check(TokenType.RIGHT_BRACE) && !IsAtEnd())
        {
            statements.Add(Declaration());
        }

        Consume(TokenType.RIGHT_BRACE, "Expected '}' after block.");
        return statements;
    }

    private PrintStmt PrintStatement()
    {
        var value = Expression();
        Consume(TokenType.SEMICOLON, "Expected ';' after value.");
        return new PrintStmt(value);
    }

    private ExpressionStmt ExpressionStatement()
    {
        var value = Expression();
        Consume(TokenType.SEMICOLON, "Expected ';' after expression.");
        return new ExpressionStmt(value);
    }

    private IfStmt IfStatement()
    {
        Consume(TokenType.LEFT_PAREN, "Expected '(' after if");
        var condition = Expression();
        Consume(TokenType.RIGHT_PAREN, "Expected ')' after if condition");

        var thenBranch = Statement();
        var elseBranch = Match(TokenType.ELSE) ? Statement() : null;

        return new IfStmt(condition, thenBranch, elseBranch);
    }

    private WhileStmt WhileStatement()
    {
        Consume(TokenType.LEFT_PAREN, "Expected '(' after while");
        var condition = Expression();
        Consume(TokenType.RIGHT_PAREN, "Expected ')' after while condition");

        var body = Statement();

        return new WhileStmt(condition, body);
    }

    private Stmt ForStatement()
    {
        Consume(TokenType.LEFT_PAREN, "Expected '(' after for");
        Stmt? initializer;
        if (Match(TokenType.SEMICOLON))
        {
            initializer = null;
        }
        else if (Match(TokenType.VAR))
        {
            initializer = VarDeclaration();
        }
        else
        {
            initializer = ExpressionStatement();
        }

        Expr? condition = null;
        if (!Check(TokenType.SEMICOLON))
        {
            condition = Expression();
        }
        Consume(TokenType.SEMICOLON, "Expected ';' after loop condition.");

        Expr? increment = null;
        if (!Check(TokenType.RIGHT_PAREN))
        {
            increment = Expression();
        }
        Consume(TokenType.RIGHT_PAREN, "Expected ')' after for clauses.");

        Stmt body = Statement();

        if (increment != null)
        {
            body = new BlockStmt([
                    body,
                    new ExpressionStmt(increment)
                ]);
        }


        if (condition == null)
        {
            condition = new LiteralExpr(true);
        }
        body = new WhileStmt(condition, body);

        if (initializer != null)
        {
            body = new BlockStmt([
                initializer,
                body
            ]);
        }

        return body;
    }

    private ReturnStmt ReturnStatement()
    {
        var keyword = Previous();
        Expr? value = !Check(TokenType.SEMICOLON) ? Expression() : null;

        Consume(TokenType.SEMICOLON, "Expected ';' after return value.");
        return new ReturnStmt(keyword, value);
    }

    private Expr Expression()
    {
        return Assignment();
    }

    private Expr Assignment()
    {
        var expr = LogicalOr();

        if (Match(TokenType.EQUAL))
        {
            var equals = Previous();
            var value = Assignment();

            if (expr is VariableExpr vExpr)
            {
                return new AssignmentExpr(vExpr.Name, value);
            }
            else if (expr is GetExpr gExpr)
            {
                return new SetExpr(gExpr.Obj, gExpr.Name, value);
            }

            Error(equals, "Invalid assignment target.");
        }

        return expr;
    }

    private Expr Ternary()
    {
        var expr = Equality();

        if (Match(TokenType.Q_MARK))
        {
            var ifTrue = Expression();

            if (Match(TokenType.COLON))
            {
                var ifFalse = Expression();
                expr = new TernaryExpr(expr, ifTrue, ifFalse);
            }
        }

        return expr;
    }

    private Expr LogicalOr()
    {
        var expr = LogicalAnd();

        while (Match(TokenType.OR))
        {
            var token = Previous();
            var right = LogicalAnd();
            expr = new LogicalExpr(expr, token, right);
        }

        return expr;
    }

    private Expr LogicalAnd()
    {
        var expr = Equality();

        while (Match(TokenType.AND))
        {
            var token = Previous();
            var right = Equality();
            expr = new LogicalExpr(expr, token, right);
        }

        return expr;
    }

    private Expr Equality()
    {
        var expr = Comparison();

        while (Match(TokenType.BANG_EQUAL, TokenType.EQUAL_EQUAL))
        {
            var op = Previous();
            var right = Comparison();
            expr = new BinaryExpr(expr, op, right);
        }

        return expr;
    }

    private Expr Comparison()
    {
        var expr = Term();

        while (Match(TokenType.GREATER, TokenType.GREATER_EQUAL, TokenType.LESS, TokenType.LESS_EQUAL))
        {
            var op = Previous();
            var right = Term();
            expr = new BinaryExpr(expr, op, right);
        }

        return expr;
    }

    private Expr Term()
    {
        var expr = Factor();

        while (Match(TokenType.PLUS, TokenType.MINUS, TokenType.PERC))
        {
            var op = Previous();
            var right = Factor();
            expr = new BinaryExpr(expr, op, right);
        }

        return expr;
    }

    private Expr Factor()
    {
        var expr = Unary();

        while (Match(TokenType.SLASH, TokenType.STAR))
        {
            var op = Previous();
            var right = Unary();
            expr = new BinaryExpr(expr, op, right);
        }

        return expr;
    }

    private Expr Unary()
    {
        while (Match(TokenType.BANG, TokenType.MINUS))
        {
            var op = Previous();
            var right = Unary();
            return new UnaryExpr(op, right);
        }

        return Call();
    }

    private Expr Call()
    {
        var expr = Primary();

        while (true)
        {
            if (Match(TokenType.LEFT_PAREN))
            {
                expr = FinishCall(expr);
            }
            else if (Match(TokenType.DOT))
            {
                var name = Consume(TokenType.IDENTIFIER, "Expected property name after ','");
                expr = new GetExpr(expr, name);
            }
            else
            {
                break;
            }
        }

        return expr;
    }

    private CallExpr FinishCall(Expr callee)
    {
        var arguments = new List<Expr>();
        if (!Check(TokenType.RIGHT_PAREN))
        {
            do
            {
                if (arguments.Count() >= 255)
                {
                    Error(Peek(), "Can't have more than 255 arguments.");
                }

                arguments.Add(Expression());
            } while (Match(TokenType.COMMA));
        }

        Token paren = Consume(TokenType.RIGHT_PAREN, "Expect ')' after arguments.");

        return new CallExpr(callee, paren, arguments);
    }

    private Expr Primary()
    {
        if (Match(TokenType.FALSE))
        {
            return new LiteralExpr(false);
        }

        if (Match(TokenType.TRUE))
        {
            return new LiteralExpr(true);
        }

        if (Match(TokenType.NIL))
        {
            return new LiteralExpr();
        }

        if (Match(TokenType.NUMBER, TokenType.STRING) &&
            Previous() is LiteralToken ltToken)
        {
            return new LiteralExpr(ltToken.Literal);
        }

        if (Match(TokenType.SUPER))
        {
            var keyword = Previous();
            Consume(TokenType.DOT, "Expected '.' after 'super'");
            var method = Consume(TokenType.IDENTIFIER, "Expected superclass method name");

            return new SuperExpr(keyword, method);
        }

        if (Match(TokenType.THIS))
        {
            return new ThisExpr(Previous());
        }

        if (Match(TokenType.IDENTIFIER))
        {
            return new VariableExpr(Previous());
        }

        if (Match(TokenType.LEFT_PAREN))
        {
            Expr expr = Expression();
            Consume(TokenType.RIGHT_PAREN, "Expect ')' after expression.");
            return new GroupingExpr(expr);
        }

        //If we find a binary operator but no left binary expression, consume the right-hand expression and throw an error.
        if (Match(TokenType.EQUAL_EQUAL, TokenType.BANG_EQUAL, TokenType.GREATER, TokenType.GREATER_EQUAL,
                  TokenType.LESS, TokenType.LESS_EQUAL, TokenType.SLASH, TokenType.STAR, TokenType.MINUS, TokenType.PLUS))
        {
            Token peekToken = Previous();
            Expression(); //Discard right-hand expression.
            throw Error(peekToken, "Expected expression before binary operator.");
        }

        throw Error(Peek(), "Expect expression.");
    }

    private bool Match(params TokenType[] types)
    {
        if (types.Any(Check))
        {
            Advance();
            return true;
        }

        return false;
    }

    private bool Check(TokenType type) => !IsAtEnd() && Peek().Type == type;

    private Token Advance()
    {
        if (!IsAtEnd())
        {
            _current++;
        }
        return Previous();
    }

    private bool IsAtEnd() => Peek().Type == TokenType.EOF;

    private Token Peek() => _tokens.ElementAt(_current);

    private Token Previous() => _tokens.ElementAt(_current - 1);

    private Token Consume(TokenType type, string message)
    {
        if (Check(type))
        {
            return Advance();
        }

        throw Error(Peek(), message);
    }

    private ParseError Error(Token token, String message)
    {
        LoxProgram.Error(token, message);
        return new ParseError();
    }

    private void Synchronize()
    {
        Advance();

        while (!IsAtEnd())
        {
            if (Previous().Type == TokenType.SEMICOLON)
            {
                return;
            }

            switch (Peek().Type)
            {
                case TokenType.CLASS:
                case TokenType.FUN:
                case TokenType.VAR:
                case TokenType.FOR:
                case TokenType.IF:
                case TokenType.WHILE:
                case TokenType.PRINT:
                case TokenType.RETURN:
                    return;
            }

            Advance();
        }
    }

}
