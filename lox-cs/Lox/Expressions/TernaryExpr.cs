﻿using Lox.Expressions.Visitors;
using Lox.Tokens;

namespace Lox.Expressions;

public record class TernaryExpr(Expr Cond, Expr ExprIfTrue, Expr ExprIfFalse) : Expr
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}