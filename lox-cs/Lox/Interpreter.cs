﻿using System.Linq.Expressions;
using Lox.RuntimeTypes;
using Lox.Expressions;
using Lox.Statements;
using Lox.Tokens;
using System.Threading.Tasks.Dataflow;

namespace Lox;

public class Interpreter :
    Lox.Expressions.Visitors.IVisitor<object?>,
    Lox.Statements.Visitors.IVisitor<object?>
{
    private readonly Dictionary<Expr, int> _locals = [];
    public Environment Environment;
    public readonly Environment Global;

    public Interpreter()
    {
        Global = new();
        Environment = Global;

        Global.Define("Clock", new ClockCallable());
    }

    public void Interpret(IEnumerable<Stmt> statements)
    {
        try
        {
            foreach (Stmt statement in statements)
            {
                Execute(statement);
            }
        }
        catch (RuntimeError error)
        {
            LoxProgram.RuntimeError(error);
        }
    }

    public object? Visit(BinaryExpr expr)
    {
        var left = Evaluate(expr.Left);
        var right = Evaluate(expr.Right);

        switch (expr.Op.Type)
        {
            case TokenType.GREATER:
                {
                    if (left is double castedLeftDouble && right is double castedRightDouble)
                    {
                        return castedLeftDouble > (double)castedRightDouble;
                    }

                    if (left is string castedLeftString && right is string castedRightString)
                    {
                        return castedLeftString.CompareTo(castedRightString) > 0;
                    }

                    throw new RuntimeError(expr.Op, "Operands must be two numbers or two strings.");
                }
            case TokenType.GREATER_EQUAL:
                {
                    if (left is double castedLeftDouble && right is double castedRightDouble)
                    {
                        return castedLeftDouble >= (double)castedRightDouble;
                    }

                    if (left is string castedLeftString && right is string castedRightString)
                    {
                        return castedLeftString.CompareTo(castedRightString) >= 0;
                    }

                    throw new RuntimeError(expr.Op, "Operands must be two numbers or two strings.");
                }
            case TokenType.LESS:
                {
                    if (left is double castedLeftDouble && right is double castedRightDouble)
                    {
                        return castedLeftDouble < (double)castedRightDouble;
                    }

                    if (left is string castedLeftString && right is string castedRightString)
                    {
                        return castedLeftString.CompareTo(castedRightString) < 0;
                    }

                    throw new RuntimeError(expr.Op, "Operands must be two numbers or two strings.");
                }
            case TokenType.LESS_EQUAL:
                {
                    if (left is double castedLeftDouble && right is double castedRightDouble)
                    {
                        return castedLeftDouble <= (double)castedRightDouble;
                    }

                    if (left is string castedLeftString && right is string castedRightString)
                    {
                        return castedLeftString.CompareTo(castedRightString) <= 0;
                    }

                    throw new RuntimeError(expr.Op, "Operands must be two numbers or two strings.");
                }
            case TokenType.BANG_EQUAL:
                return !IsEqual(left, right);
            case TokenType.EQUAL_EQUAL:
                return IsEqual(left, right);
            case TokenType.PLUS:
                {
                    if (left is double castedLeftDouble && right is double castedRightDouble)
                    {
                        return castedLeftDouble + castedRightDouble;
                    }

                    if (left is string castedLeftString)
                    {
                        return castedLeftString + right?.ToString();
                    }

                    if (right is string castedRightString)
                    {
                        return left?.ToString() + right;
                    }

                    throw new RuntimeError(expr.Op, "Operands must be two numbers or two strings.");
                }
            case TokenType.MINUS:
                CheckNumberOperands(expr.Op, left, right);
                return (double)left - (double)right;
            case TokenType.STAR:
                {
                    if (left is double castedLeftDouble && right is double castedRightDouble)
                    {
                        return castedLeftDouble * castedRightDouble;
                    }

                    if (left is string castedLeftString && right is double castedRightDouble1)
                    {
                        return string.Concat(Enumerable.Repeat(castedLeftString, (int)castedRightDouble1));
                    }

                    if (left is double castedLeftDouble1 && right is double castedRightString)
                    {
                        return string.Concat(Enumerable.Repeat(castedRightString, (int)castedLeftDouble1));
                    }

                    throw new RuntimeError(expr.Op, "Operands must be two numbers or two strings.");
                }
            case TokenType.SLASH:
                CheckNumberOperands(expr.Op, left, right);
                return (double)left / (double)right;
            case TokenType.PERC:
                CheckNumberOperands(expr.Op, left, right);
                return (double)left % (double)right;
            default:
                return null;
        }
    }

    public object? Visit(GroupingExpr expr) => Evaluate(expr.Expression);

    public object? Visit(LiteralExpr expr) => expr.Value;

    public object? Visit(UnaryExpr expr)
    {
        var right = Evaluate(expr.Right);
        if (right == null)
        {
            return null;
        }

        switch (expr.Op.Type)
        {
            case TokenType.MINUS:
                CheckNumberOperand(expr.Op, right);
                return -(double)right;
            case TokenType.BANG:
                return !IsTruthy(right);
            default:
                return null;
        }
    }

    public object? Visit(TernaryExpr ternaryExpr)
    {
        var cond = Evaluate(ternaryExpr.Cond);

        return Evaluate(IsTruthy(cond) ?
                            ternaryExpr.ExprIfTrue :
                            ternaryExpr.ExprIfFalse);
    }

    public object? Visit(VariableExpr variableExpr)
    {
        return LookUpVariable(variableExpr.Name, variableExpr);
    }

    public object? Visit(AssignmentExpr expr)
    {
        var value = Evaluate(expr.Value);
        if (_locals.TryGetValue(expr, out var distance))
        {
            Environment.AssignAt(distance, expr.Name, value);
        }
        else
        {
            Global.Assign(expr.Name, value);
        }

        return value;
    }

    public object? Visit(LogicalExpr logicalExpr)
    {
        var left = Evaluate(logicalExpr.Left);

        if (logicalExpr.Op.Type == TokenType.OR)
        {
            if (IsTruthy(left)) return left;
        }
        else
        {
            if (!IsTruthy(left)) return left;
        }

        return Evaluate(logicalExpr.Right);
    }

    public object? Visit(CallExpr callExpr)
    {
        var callee = Evaluate(callExpr.Callee);

        if (callee == null)
        {
            throw new RuntimeError(callExpr.Paren, "No callable to call.");
        }

        if (callee is not ILoxCallable function)
        {
            throw new RuntimeError(callExpr.Paren, "Can only call functions and classes.");
        }

        var arguments = callExpr.Arguments.Select(Evaluate).ToList();
        if (arguments.Count != function.Arity())
        {
            throw new RuntimeError(callExpr.Paren, $"Expected {function.Arity()} arguments but got {arguments.Count}");
        }

        return function.Call(this, arguments);
    }

    public object? Visit(GetExpr getExpr)
    {
        var obj = Evaluate(getExpr.Obj);

        if (obj is LoxInstance inst)
        {
            return inst.Get(getExpr.Name);
        }

        throw new RuntimeError(getExpr.Name, "Only instances have properties.");
    }

    public object? Visit(SetExpr setExpr)
    {
        var obj = Evaluate(setExpr.Obj);

        if (obj is not LoxInstance inst)
        {
            throw new RuntimeError(setExpr.Name, "Only instances have fields.");
        }

        var value = Evaluate(setExpr.Value);
        inst.Set(setExpr.Name, value);

        return value;
    }

    public object? Visit(ThisExpr thisExpr)
    {
        return LookUpVariable(thisExpr.Keyword, thisExpr);
    }

    public object? Visit(SuperExpr superExpr)
    {
        var distance = _locals[superExpr];
        if (Environment.GetAt(distance, "super") is not LoxClass superclass)
        {
            throw new RuntimeError(superExpr.Method, "Cannot access base class");
        }
        if (Environment.GetAt(distance - 1, "this") is not LoxInstance obj)
        {
            throw new RuntimeError(superExpr.Method, "Cannot access current object instance");
        }

        var method = superclass.FindMethod(superExpr.Method.Lexeme) ?? throw new RuntimeError(superExpr.Method, $"Cannot access method {superExpr.Method.Lexeme} in base class");
        return method.Bind(obj);
    }

    public object? Visit(ExpressionStmt stmt)
    {
        return Evaluate(stmt.Expression);
    }

    public object? Visit(PrintStmt stmt)
    {
        var value = Evaluate(stmt.Expression);
        Console.WriteLine(Stringify(value));

        return null;
    }

    public object? Visit(VarStmt stmt)
    {
        if (stmt.Initializer == null)
        {
            Environment.Define(stmt.Name.Lexeme);
        }
        else
        {
            Environment.Define(stmt.Name.Lexeme, Evaluate(stmt.Initializer));
        }

        return null;
    }

    public object? Visit(BlockStmt stmt)
    {
        ExecuteBlock(stmt.Statements, new Environment(Environment));
        return null;
    }

    public object? Visit(IfStmt ifStmt)
    {
        if (IsTruthy(Evaluate(ifStmt.Condition)))
        {
            Execute(ifStmt.ThenBranch);
        }
        else if (ifStmt.ElseBranch != null)
        {
            Execute(ifStmt.ElseBranch);
        }

        return null;
    }

    public object? Visit(WhileStmt whileStmt)
    {
        while (IsTruthy(Evaluate(whileStmt.Condition)))
        {
            Execute(whileStmt.Body);
        }

        return null;
    }

    public object? Visit(FunctionStmt functionStmt)
    {
        var function = new LoxFunction(functionStmt, Environment, false);
        Environment.Define(functionStmt.Name.Lexeme, function);

        return null;
    }

    public object? Visit(ReturnStmt returnStmt)
    {
        var value = returnStmt.Value == null ? null : Evaluate(returnStmt.Value);

        throw new Return(value);
    }

    public object? Visit(ClassStmt classStmt)
    {
        LoxClass? superclass = null;
        if (classStmt.Superclass != null &&
            Evaluate(classStmt.Superclass) is var sClass)
        {
            if (superclass is LoxClass s)
            {
                superclass = s;
            }
            else
            {
                throw new RuntimeError(classStmt.Superclass.Name, "Superclass must be a class.");
            }
        }

        Environment.Define(classStmt.Name.Lexeme, null);

        if (classStmt.Superclass != null)
        {
            Environment = new Environment(Environment);
            Environment.Define("super", superclass);
        }

        var methods = new Dictionary<string, LoxFunction>();
        foreach (var m in classStmt.Methods)
        {
            var func = new LoxFunction(m, Environment, m.Name.Lexeme.Equals("init"));
            methods.TryAdd(m.Name.Lexeme, func);
        }

        var klass = new LoxClass(classStmt.Name.Lexeme, superclass, methods);

        if (classStmt.Superclass != null)
        {
            Environment = Environment.Enclosing!;
        }

        Environment.Assign(classStmt.Name, klass);

        return null;
    }

    private object? Evaluate(Expr expression) => expression.Accept(this);

    private static bool IsEqual(object a, object b)
    {
        if (a == null && b == null)
        {
            return true;
        }

        if (a == null)
        {
            return false;
        }

        return a.Equals(b);
    }

    private static bool IsTruthy(object? expression)
    {
        if (expression is null)
        {
            return false;
        }

        if (expression is bool castedBool)
        {
            return castedBool;
        }

        return true;
    }

    private static void CheckNumberOperand(Token op, object? operand)
    {
        if (operand is double)
        {
            return;
        }

        throw new RuntimeError(op, "Operand must be a number.");
    }

    private static void CheckNumberOperands(Token op, object? left, object? right)
    {
        if (left is double && right is double)
        {
            return;
        }

        throw new RuntimeError(op, "Operand must be a number.");
    }

    private static string? Stringify(object? o)
    {
        if (o == null)
        {
            return "nil";
        }

        if (o is double d)
        {
            string text = d.ToString();
            if (text.EndsWith(".0"))
            {
                text = text[..^2];
            }
            return text;
        }

        return o?.ToString();
    }

    private void Execute(Stmt stmt)
    {
        stmt.Accept(this);
    }

    public void ExecuteBlock(List<Stmt?> statements, Environment environment)
    {
        Environment previous = Environment;
        try
        {
            Environment = environment;

            foreach (var statement in statements.Where(s => s != null))
            {
                Execute(statement!);
            }
        }
        finally
        {
            Environment = previous;
        }
    }

    public void Resolve(Expr expr, int depth)
    {
        _locals[expr] = depth;
    }

    private object? LookUpVariable(Token name, Expr expr)
    {
        if (_locals.TryGetValue(expr, out int distance))
        {
            return Environment.GetAt(distance, name.Lexeme);
        }
        else
        {
            return Global.Get(name);
        }
    }
}
