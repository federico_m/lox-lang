namespace Lox.Statements.Visitors;

public interface IVisitor<R>
{
    R Visit(ExpressionStmt stmt);
    R Visit(PrintStmt stmt);
    R Visit(VarStmt stmt);
    R Visit(BlockStmt stmt);
    R Visit(IfStmt ifStmt);
    R Visit(WhileStmt whileStmt);
    R Visit(FunctionStmt functionStmt);
    R Visit(ReturnStmt returnStmt);
    R Visit(ClassStmt classStmt);
}