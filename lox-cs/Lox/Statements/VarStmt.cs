using Lox.Expressions;
using Lox.Statements.Visitors;
using Lox.Tokens;

namespace Lox.Statements;

public record class VarStmt(Token Name, Expr? Initializer) : Stmt
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}