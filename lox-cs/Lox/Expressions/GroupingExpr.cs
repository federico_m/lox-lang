﻿using Lox.Expressions.Visitors;

namespace Lox.Expressions;

public record class GroupingExpr(Expr Expression) : Expr
{
        public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}
