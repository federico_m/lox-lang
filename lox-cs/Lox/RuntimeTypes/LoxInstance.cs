using Lox.Tokens;

namespace Lox.RuntimeTypes;

public record class LoxInstance(LoxClass Klass)
{
    private readonly Dictionary<string, object?> _fields = [];

    public override string ToString()
    {
        return $"{Klass.Name} instance";
    }

    internal object? Get(Token name)
    {
        if (_fields.TryGetValue(name.Lexeme, out var f))
        {
            return f;
        }

        if (Klass.FindMethod(name.Lexeme) is LoxFunction method)
        {
            return method.Bind(this);
        }

        throw new RuntimeError(name, $"Undefined property '{name.Lexeme}'.");
    }

    internal void Set(Token name, object? value)
    {
        _fields[name.Lexeme] = value;
    }
}