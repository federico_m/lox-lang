using Lox.Tokens;

namespace Lox;

public class Return(object? value) : Exception(null, null)
{
    public readonly object? Value = value;
}