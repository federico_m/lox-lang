﻿using Lox.Expressions.Visitors;

namespace Lox.Expressions;

public abstract record class Expr
{
    public abstract R Accept<R>(IVisitor<R> visitor);
}
