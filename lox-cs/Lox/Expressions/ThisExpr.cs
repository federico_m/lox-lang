using Lox.Expressions.Visitors;
using Lox.Tokens;

namespace Lox.Expressions;

public record class ThisExpr(Token Keyword) : Expr
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}