using Lox.Statements.Visitors;

namespace Lox.Statements;

public record class BlockStmt(List<Stmt?> Statements) : Stmt
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}