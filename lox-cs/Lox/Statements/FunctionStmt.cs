using Lox.Expressions;
using Lox.Statements.Visitors;
using Lox.Tokens;

namespace Lox.Statements;

public record class FunctionStmt(Token Name, List<Token> Params, List<Stmt?> Body) : Stmt
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}