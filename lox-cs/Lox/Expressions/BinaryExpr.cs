using Lox.Expressions.Visitors;
using Lox.Tokens;

namespace Lox.Expressions;

public record class BinaryExpr(Expr Left, Token Op, Expr Right) : Expr
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}
