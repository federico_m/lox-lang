using Lox.Expressions.Visitors;
using Lox.Tokens;

namespace Lox.Expressions;

public record class AssignmentExpr(Token Name, Expr Value) : Expr
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}