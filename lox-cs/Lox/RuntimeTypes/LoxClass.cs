
namespace Lox.RuntimeTypes;

public record class LoxClass(string Name, LoxClass? Superclass, Dictionary<string, LoxFunction> Methods) : ILoxCallable
{
    public int Arity()
    {
        if (FindMethod("init") is LoxFunction initializer)
        {
            return initializer.Arity();
        }

        return 0;
    }

    public object? Call(Interpreter interpreter, List<object?> arguments)
    {
        var instance = new LoxInstance(this);

        if (FindMethod("init") is LoxFunction initializer)
        {
            initializer.Bind(instance).Call(interpreter, arguments);
        }

        return instance;
    }

    public override string ToString()
    {
        return Name;
    }

    public LoxFunction? FindMethod(string name)
    {
        if (Methods.TryGetValue(name, out var method))
        {
            return method;
        }

        if (Superclass != null)
        {
            return Superclass.FindMethod(name);
        }

        return null;
    }
}