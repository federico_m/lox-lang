using System.ComponentModel;
using Lox.Tokens;

namespace Lox;

public class Environment(Environment? enclosing = null)
{
    private readonly Dictionary<string, (object?, bool)> _values = [];
    public readonly Environment? Enclosing = enclosing;

    public void Define(string name, object? value) => InnerDefine(name, value, true);
    public void Define(string name) => InnerDefine(name, null, false);

    private void InnerDefine(string name, object? value, bool initialized = true)
    {
        if (!_values.ContainsKey(name))
        {
            _values.Add(name, (value, initialized));
            return;
        }

        _values[name] = (value, initialized);
    }

    public object? Get(Token name)
    {
        if (_values.ContainsKey(name.Lexeme))
        {
            var data = _values[name.Lexeme];

            if (!data.Item2)
            {
                throw new RuntimeError(name, "Variable '" + name.Lexeme + "' has not been initialized.");
            }

            return data.Item1;
        }

        if (Enclosing != null)
        {
            return Enclosing.Get(name);
        }

        throw new RuntimeError(name, "Undefined variable '" + name.Lexeme + "'.");
    }
    public object? GetAt(int distance, string name)
    {
        var data = (Ancestor(distance)?._values[name]) ?? throw new RuntimeError("Undefined variable '" + name + "'.");
        if (!data.Item2)
        {
            throw new RuntimeError("Variable '" + name + "' has not been initialized.");
        }

        return data.Item1;
    }

    internal void Assign(Token name, object? value)
    {
        if (_values.ContainsKey(name.Lexeme))
        {
            _values[name.Lexeme] = (value, true);
            return;
        }

        if (Enclosing != null)
        {
            Enclosing.Assign(name, value);
            return;
        }

        throw new RuntimeError(name, "Undefined variable '" + name.Lexeme + "'.");
    }

    internal void AssignAt(int distance, Token name, object? value)
    {
        var data = (Ancestor(distance)?._values) ?? throw new RuntimeError(name, "Cannot find referenced environment.");

        data[name.Lexeme] = (value, true);
    }


    private Environment? Ancestor(int distance)
    {
        Environment? environment = this;

        for (int i = 0; i < distance; i++)
        {
            environment = environment?.Enclosing;
        }

        return environment;
    }
}