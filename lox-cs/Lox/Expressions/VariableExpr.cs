using Lox.Expressions.Visitors;
using Lox.Tokens;

namespace Lox.Expressions;

public record class VariableExpr(Token Name) : Expr
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}
