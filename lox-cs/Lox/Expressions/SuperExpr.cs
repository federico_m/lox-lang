using Lox.Expressions.Visitors;
using Lox.Tokens;

namespace Lox.Expressions;

public record class SuperExpr(Token Keyword, Token Method) : Expr
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}