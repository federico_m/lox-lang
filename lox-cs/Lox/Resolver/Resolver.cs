using System.Diagnostics.Tracing;
using Lox.Expressions;
using Lox.Statements;
using Lox.Tokens;

namespace Lox.Resolver;

public class Resolver(Interpreter interpreter) :
    Lox.Expressions.Visitors.IVisitor<object?>,
    Lox.Statements.Visitors.IVisitor<object?>
{
    private readonly Interpreter _interpreter = interpreter;
    private readonly Stack<Dictionary<string, bool>> _scopes = new();

    private FunctionType _currentFunction = FunctionType.NONE;
    private ClassType _currentClass = ClassType.NONE;

    public object? Visit(BinaryExpr expr)
    {
        Resolve(expr.Left);
        Resolve(expr.Right);

        return null;
    }

    public object? Visit(GroupingExpr expr)
    {
        Resolve(expr.Expression);

        return null;
    }

    public object? Visit(LiteralExpr expr)
    {
        return null;
    }

    public object? Visit(UnaryExpr expr)
    {
        Resolve(expr.Right);

        return null;
    }

    public object? Visit(TernaryExpr expr)
    {
        Resolve(expr.Cond);
        Resolve(expr.ExprIfTrue);
        Resolve(expr.ExprIfFalse);

        return null;
    }

    public object? Visit(VariableExpr variableExpr)
    {
        if (_scopes.Any() &&
            _scopes.Peek().TryGetValue(variableExpr.Name.Lexeme, out var result) &&
            !result)
        {
            LoxProgram.Error(variableExpr.Name, "Can't read local variable in its own initializer.");
        }

        ResolveLocal(variableExpr, variableExpr.Name);

        return null;
    }

    public object? Visit(AssignmentExpr expr)
    {
        Resolve(expr.Value);
        ResolveLocal(expr, expr.Name);
        return null;
    }

    public object? Visit(LogicalExpr expr)
    {
        Resolve(expr.Left);
        Resolve(expr.Right);

        return null;
    }

    public object? Visit(CallExpr expr)
    {
        Resolve(expr.Callee);

        foreach (var arg in expr.Arguments)
        {
            Resolve(arg);
        }

        return null;
    }

    public object? Visit(ExpressionStmt stmt)
    {
        Resolve(stmt.Expression);
        return null;
    }

    public object? Visit(PrintStmt stmt)
    {
        Resolve(stmt.Expression);
        return null;
    }

    public object? Visit(VarStmt stmt)
    {
        Declare(stmt.Name);
        if (stmt.Initializer != null)
        {
            Resolve(stmt.Initializer);
        }
        Define(stmt.Name);
        return null;
    }

    public object? Visit(BlockStmt stmt)
    {
        BeginScope();
        Resolve(stmt.Statements);
        EndScope();

        return null;
    }

    public object? Visit(IfStmt stmt)
    {
        Resolve(stmt.Condition);
        Resolve(stmt.ThenBranch);
        if (stmt.ElseBranch != null)
        {
            Resolve(stmt.ElseBranch);
        }

        return null;
    }

    public object? Visit(WhileStmt stmt)
    {
        Resolve(stmt.Condition);
        Resolve(stmt.Body);

        return null;
    }

    public object? Visit(FunctionStmt stmt)
    {
        Declare(stmt.Name);
        Define(stmt.Name);

        ResolveFunction(stmt, FunctionType.FUNCTION);
        return null;
    }

    public object? Visit(ReturnStmt stmt)
    {
        if (_currentFunction == FunctionType.NONE)
        {
            LoxProgram.Error(stmt.Keyword, "Can't return from top-level code.");
        }

        if (_currentFunction == FunctionType.INITIALIZER)
        {
            LoxProgram.Error(stmt.Keyword, "Can't return a value in an initilizer.");
        }

        if (stmt.Value != null)
        {
            Resolve(stmt.Value);
        }

        return null;
    }

    public object? Visit(ClassStmt classStmt)
    {
        var enclosingClass = _currentClass;
        _currentClass = ClassType.CLASS;

        Declare(classStmt.Name);
        Define(classStmt.Name);

        if (classStmt.Superclass != null)
        {
            if (classStmt.Name.Lexeme.Equals(classStmt.Superclass.Name))
            {
                LoxProgram.Error(classStmt.Superclass.Name, "A class connot inherit form itself.");
            }

            _currentClass = ClassType.SUBCLASS;
            Resolve(classStmt.Superclass);

            BeginScope();
            _scopes.Peek().Add("super", true);
        }

        BeginScope();
        _scopes.Peek().Add("this", true);

        foreach (var method in classStmt.Methods)
        {
            var declaration = method.Name.Lexeme.Equals("init") ? FunctionType.INITIALIZER : FunctionType.METHOD;
            ResolveFunction(method, declaration);
        }

        EndScope();

        if (classStmt.Superclass != null)
        {
            EndScope();
        }

        _currentClass = enclosingClass;

        return null;
    }

    public object? Visit(GetExpr getExpr)
    {
        Resolve(getExpr.Obj);
        return null;
    }

    public object? Visit(SetExpr setExpr)
    {
        Resolve(setExpr.Value);
        Resolve(setExpr.Obj);
        return null;
    }

    public object? Visit(ThisExpr thisExpr)
    {
        if (_currentClass != ClassType.CLASS)
        {
            LoxProgram.Error(thisExpr.Keyword, "Can't use 'this' outside of a class.");
            return null;
        }

        ResolveLocal(thisExpr, thisExpr.Keyword);
        return null;
    }

    public object? Visit(SuperExpr superExpr)
    {
        if (_currentClass == ClassType.NONE)
        {
            LoxProgram.Error(superExpr.Keyword, "Can't use 'super' outside of a class.");
        }
        else if (_currentClass != ClassType.SUBCLASS)
        {
            LoxProgram.Error(superExpr.Keyword, "Can't use 'super' in a class with no superclass.");
        }

        ResolveLocal(superExpr, superExpr.Keyword);
        return null;
    }

    private void BeginScope()
    {
        _scopes.Push([]);
    }

    private void EndScope()
    {
        _scopes.Pop();
    }

    public void Resolve(List<Stmt?> statements)
    {
        foreach (var statement in statements)
        {
            Resolve(statement);
        }
    }

    private void Resolve(Stmt? stmt)
    {
        stmt?.Accept(this);
    }

    private void Resolve(Expr? expr)
    {
        expr?.Accept(this);
    }

    private void ResolveLocal(Expr expr, Token name)
    {
        for (int i = 0; i < _scopes.Count; i++)
        {
            if (_scopes.ElementAt(i).ContainsKey(name.Lexeme))
            {
                _interpreter.Resolve(expr, i);
                return;
            }
        }
    }

    private void ResolveFunction(FunctionStmt function, FunctionType type)
    {
        var enclosingFunction = _currentFunction;
        _currentFunction = type;

        BeginScope();
        foreach (Token param in function.Params)
        {
            Declare(param);
            Define(param);
        }
        Resolve(function.Body);
        EndScope();

        _currentFunction = enclosingFunction;
    }

    private void Declare(Token name)
    {
        if (!_scopes.Any())
        {
            return;
        }

        var scope = _scopes.Peek();
        if (scope.ContainsKey(name.Lexeme))
        {
            LoxProgram.Error(name, "Already a variable with this name in this scope.");
        }
        scope[name.Lexeme] = false;
    }

    private void Define(Token name)
    {
        if (!_scopes.Any())
        {
            return;
        }

        var scope = _scopes.Peek();
        scope[name.Lexeme] = true;
    }
}
