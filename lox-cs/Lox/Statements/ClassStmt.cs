using Lox.Expressions;
using Lox.Statements.Visitors;
using Lox.Tokens;

namespace Lox.Statements;

public record class ClassStmt(Token Name, List<FunctionStmt> Methods, VariableExpr? Superclass) : Stmt
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}