namespace Lox.Tokens;

public record class LiteralToken(TokenType Type, string Lexeme, object? Literal, int Line) : Token(Type, Lexeme, Line)
{
    private readonly TokenType _type = Type;
    private readonly string _lexeme = Lexeme;
    private readonly object? _literal = Literal;
    private readonly int _line = Line;

    public override string ToString() => $"[{_line}] {_type} {_lexeme} {_literal}";
}
