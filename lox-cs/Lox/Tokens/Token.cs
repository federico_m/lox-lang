﻿namespace Lox.Tokens;

public record class Token(TokenType Type, string Lexeme, int Line)
{
    private readonly TokenType _type = Type;
    private readonly string _lexeme = Lexeme;
    private readonly int _line = Line;
    public override string ToString() => $"[{_line}] {_type} {_lexeme}";
}
