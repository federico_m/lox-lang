using Lox.Expressions.Visitors;
using Lox.Tokens;

namespace Lox.Expressions;

public record class CallExpr(Expr Callee, Token Paren, List<Expr> Arguments) : Expr
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}
