
namespace Lox.RuntimeTypes;

public sealed record class ClockCallable : ILoxCallable
{
    public int Arity() => 0;

    public object? Call(Interpreter interpreter, List<object?> arguments) => DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
}