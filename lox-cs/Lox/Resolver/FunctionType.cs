namespace Lox.Resolver;

public enum FunctionType
{
    NONE,
    FUNCTION,
    METHOD,
    INITIALIZER
}