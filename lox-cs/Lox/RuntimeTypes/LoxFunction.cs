
using System.Linq.Expressions;
using Lox.Statements;

namespace Lox.RuntimeTypes;

public sealed class LoxFunction(FunctionStmt declaration, Environment closure, bool isInitializer) : ILoxCallable
{
    private readonly Environment _closure = closure;
    private readonly FunctionStmt _declaration = declaration;
    private readonly bool _isInitializer = isInitializer;
    public int Arity() => _declaration.Params.Count;

    public object? Call(Interpreter interpreter, List<object?> arguments)
    {
        var env = new Environment(_closure);

        for (var i = 0; i < _declaration.Params.Count; i++)
        {
            env.Define(_declaration.Params[i].Lexeme, arguments[i]);
        }

        try
        {
            interpreter.ExecuteBlock(_declaration.Body, env);
        }
        catch (Return ret)
        {
            if (_isInitializer)
            {
                return _closure.GetAt(0, "this");
            }
            return ret.Value;
        }

        if (_isInitializer)
        {
            return _closure.GetAt(0, "this");
        }
        return null;
    }

    public override string ToString() => $"<fn {_declaration.Name.Lexeme}>";

    internal LoxFunction Bind(LoxInstance loxInstance)
    {
        var environment = new Environment(_closure);
        environment.Define("this", loxInstance);
        return new LoxFunction(_declaration, environment, _isInitializer);
    }
}