namespace Lox.Resolver;

public enum ClassType
{
    NONE,
    CLASS,
    SUBCLASS,
}
