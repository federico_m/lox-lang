using Lox.Expressions;
using Lox.Statements.Visitors;
using Lox.Tokens;

namespace Lox.Statements;

public record class ReturnStmt(Token Keyword, Expr? Value) : Stmt
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}