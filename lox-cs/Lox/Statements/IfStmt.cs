using Lox.Expressions;
using Lox.Statements.Visitors;

namespace Lox.Statements;

public record class IfStmt(Expr Condition, Stmt ThenBranch, Stmt? ElseBranch) : Stmt
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}