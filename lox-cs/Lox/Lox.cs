﻿using Lox.Expressions;
using Lox.Expressions.Visitors;
using Lox.Resolver;
using Lox.Tokens;

namespace Lox;

public static class LoxProgram
{
    private static readonly Interpreter _interpreter = new();

    /// <summary>
    /// Indicates whether the interpreter has had any error
    /// </summary>
    private static bool _hadError = false;

    /// <summary>
    /// Indicates whether the interpreter has had any error at runtime
    /// </summary>
    private static bool _hadRuntimeError = false;

    public static void Main(string[] args)
    {
        if (args.Length > 1)
        {
            Console.WriteLine("Usage: lox [script]");
            System.Environment.Exit(64);
        }
        else if (args.Length == 1)
        {
            RunFile(args[0]);
        }
        else
        {
            RunPrompt();
        }
    }

    /// <summary>
    /// Executes an interactive prompt
    /// </summary>
    private static void RunPrompt()
    {
        while (true)
        {
            Console.Write("> ");
            var input = Console.ReadLine();
            if (input == null)
            {
                return;
            }

            Run(input);

            // Reset the flag not to kill the entire REPL
            _hadError = false;
        }
    }

    /// <summary>
    /// Executes code contained in <paramref name="fileName"/>
    /// </summary>
    /// <param name="fileName">File name</param>
    /// <exception cref="IOException"></exception>
    private static void RunFile(string fileName)
    {
        var content = File.ReadAllText(fileName);
        Run(content);

        if (_hadError)
        {
            System.Environment.Exit(65);
        }

        if (_hadRuntimeError)
        {
            System.Environment.Exit(70);
        }
    }

    /// <summary>
    /// Executes the code contained in <paramref name="source"/>
    /// </summary>
    /// <param name="source"></param>
    private static void Run(string source)
    {
        var scanner = new Scanner(source);
        var tokens = scanner.ScanTokens();
        var parser = new Parser(tokens);
        var statements = parser.Parse();

        // Stop if there was a syntax error.
        if (_hadError || statements == null)
        {
            return;
        }

        var resolver = new Resolver.Resolver(_interpreter);
        resolver.Resolve(statements!);
        // Stop if there was a resolution error.
        if (_hadError || statements == null)
        {
            return;
        }
        _interpreter.Interpret(statements!);
    }

    /// <summary>
    /// Shows an error
    /// </summary>
    /// <param name="line">Line number in file</param>
    /// <param name="message">Message</param>
    public static void Error(int line, string message)
    {
        Report(line, string.Empty, message);
    }

    public static void Error(Token token, string message)
    {
        if (token.Type == TokenType.EOF)
        {
            Report(token.Line, " at end", message);
        }
        else
        {
            Report(token.Line, " at '" + token.Lexeme + "'", message);
        }
    }

    /// <summary>
    /// Reports an error
    /// </summary>
    /// <param name="line">Line number</param>
    /// <param name="where"></param>
    /// <param name="message">Error message</param>
    public static void Report(int line, string where, string message)
    {
        Console.WriteLine($"[line {line}] Error{where}: {message}");
        _hadError = true;
    }

    public static void RuntimeError(RuntimeError error)
    {
        Console.WriteLine($"{error.Message}\n[line {error.Token.Line}]");
        _hadRuntimeError = true;
    }
}
