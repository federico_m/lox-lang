﻿using System.ComponentModel;
using System.Globalization;
using System.Reflection.Metadata;
using Lox.Tokens;

namespace Lox;

public class Scanner(string source)
{
    private static readonly Dictionary<string, TokenType> _keywords = [];

    private readonly string _source = source;
    private readonly List<Token> _tokens = [];
    private int _start = 0;
    private int _current = 0;
    private int _line = 0;

    private bool IsAtEnd => _current >= _source.Length;

    static Scanner()
    {
        _keywords.Add("and", TokenType.AND);
        _keywords.Add("class", TokenType.CLASS);
        _keywords.Add("else", TokenType.ELSE);
        _keywords.Add("false", TokenType.FALSE);
        _keywords.Add("for", TokenType.FOR);
        _keywords.Add("fun", TokenType.FUN);
        _keywords.Add("if", TokenType.IF);
        _keywords.Add("nil", TokenType.NIL);
        _keywords.Add("or", TokenType.OR);
        _keywords.Add("print", TokenType.PRINT);
        _keywords.Add("return", TokenType.RETURN);
        _keywords.Add("super", TokenType.SUPER);
        _keywords.Add("this", TokenType.THIS);
        _keywords.Add("true", TokenType.TRUE);
        _keywords.Add("var", TokenType.VAR);
        _keywords.Add("while", TokenType.WHILE);
    }

    public List<Token> ScanTokens()
    {
        while (!IsAtEnd)
        {
            _start = _current;
            ScanToken();
        }

        _tokens.Add(new Token(TokenType.EOF, string.Empty, _line));
        return _tokens;
    }

    private void ScanToken()
    {
        char c = Advance();
        switch (c)
        {
            case '(':
                AddToken(TokenType.LEFT_PAREN);
                break;
            case ')':
                AddToken(TokenType.RIGHT_PAREN);
                break;
            case '{':
                AddToken(TokenType.LEFT_BRACE);
                break;
            case '}':
                AddToken(TokenType.RIGHT_BRACE);
                break;
            case ',':
                AddToken(TokenType.COMMA);
                break;
            case '.':
                AddToken(TokenType.DOT);
                break;
            case '-':
                AddToken(TokenType.MINUS);
                break;
            case '+':
                AddToken(TokenType.PLUS);
                break;
            case ';':
                AddToken(TokenType.SEMICOLON);
                break;
            case '*':
                AddToken(TokenType.STAR);
                break;
            case '%':
                AddToken(TokenType.PERC);
                break;
            case '?':
                AddToken(TokenType.Q_MARK);
                break;
            case ':':
                AddToken(TokenType.COLON);
                break;
            case '!':
                AddToken(Match('=') ? TokenType.BANG_EQUAL : TokenType.BANG);
                break;
            case '=':
                AddToken(Match('=') ? TokenType.EQUAL_EQUAL : TokenType.EQUAL);
                break;
            case '<':
                AddToken(Match('=') ? TokenType.LESS_EQUAL : TokenType.LESS);
                break;
            case '>':
                AddToken(Match('=') ? TokenType.GREATER_EQUAL : TokenType.GREATER);
                break;
            case '/':
                if (Match('/'))
                {
                    // A comment goes until the end of the line.
                    while (Peek() != '\n' && !IsAtEnd)
                    {
                        Advance();
                    }
                }
                else
                {
                    AddToken(TokenType.SLASH);
                }
                break;

            case ' ':
            case '\r':
            case '\t':
                // Ignore whitespace.
                break;
            case '\n':
                _line++;
                break;
            case '"':
                ParseString();
                break;
            default:
                if (IsDigit(c))
                {
                    ParseNumber();
                }
                else if (IsAlpha(c))
                {
                    ParseIdentifier();
                }
                else
                {
                    LoxProgram.Error(_line, "Unexpected character.");
                }
                break;
        }
    }
    private void ParseString()
    {
        while (Peek() != '"' && !IsAtEnd)
        {
            if (Peek() == '\n')
            {
                _line++;
            }
            Advance();
        }

        if (IsAtEnd)
        {
            LoxProgram.Error(_line, "Unterminated string.");
            return;
        }

        // The closing ".
        Advance();

        // Trim the surrounding quotes.
        var value = _source.Substring(_start + 1, _current - _start - 2);
        AddToken(TokenType.STRING, value);
    }

    private void ParseNumber()
    {
        while (IsDigit(Peek()))
        {
            Advance();
        }

        if (Peek() == '.' && IsDigit(PeekNext()))
        {
            Advance();

            while (IsDigit(Peek()))
            {
                Advance();
            }
        }

        AddToken(TokenType.NUMBER, double.Parse(_source.Substring(_start, _current - _start), CultureInfo.InvariantCulture));
    }

    private void ParseIdentifier()
    {
        while (IsAlphaNumeric(Peek()))
        {
            Advance();
        }

        var text = _source.Substring(_start, _current - _start);
        var type = _keywords.GetValueOrDefault(text, TokenType.IDENTIFIER);
        AddToken(type);
    }

    private bool Match(char expected)
    {
        if (IsAtEnd)
        {
            return false;
        }

        if (Peek() != expected)
        {
            return false;
        }

        Advance();
        return true;
    }

    private char Peek() => IsAtEnd ? '\0' : _source.ElementAt(_current);

    private char PeekNext() => _current + 1 < _source.Length ? _source.ElementAt(_current + 1) : '\0';

    private bool IsDigit(char c) => c is >= '0' and <= '9';

    private bool IsAlpha(char c) => c is >= 'a' and <= 'z' or >= 'A' and <= 'Z' or '_';

    private bool IsAlphaNumeric(char c) => IsAlpha(c) || IsDigit(c);

    private char Advance() => _source.ElementAt(_current++);

    private void AddToken(TokenType type) => AddToken(type, null);

    private void AddToken(TokenType type, object? literal = null)
    {
        var text = _source.Substring(_start, _current - _start);
        if (literal is null)
        {
            _tokens.Add(new Token(type, text, _line));
        }
        else
        {
            _tokens.Add(new LiteralToken(type, text, literal, _line));
        }
    }
}
