using Lox.Statements.Visitors;

namespace Lox.Statements;

public abstract record class Stmt
{
    public abstract R Accept<R>(IVisitor<R> visitor);
}