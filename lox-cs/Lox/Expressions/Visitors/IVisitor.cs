﻿namespace Lox.Expressions.Visitors;

public interface IVisitor<R>
{
    R Visit(BinaryExpr expr);
    R Visit(GroupingExpr expr);
    R Visit(LiteralExpr expr);
    R Visit(UnaryExpr expr);
    R Visit(TernaryExpr ternaryExpr);
    R Visit(VariableExpr variableExpr);
    R Visit(AssignmentExpr assignmentExpr);
    R Visit(LogicalExpr logicalExpr);
    R Visit(CallExpr callExpr);
    R Visit(GetExpr getExpr);
    R Visit(SetExpr setExpr);
    R Visit(ThisExpr thisExpr);
    R Visit(SuperExpr superExpr);
}
