using Lox.Expressions;
using Lox.Statements.Visitors;

namespace Lox.Statements;

public record class WhileStmt(Expr Condition, Stmt Body) : Stmt
{
    public override R Accept<R>(IVisitor<R> visitor)
    {
        return visitor.Visit(this);
    }
}